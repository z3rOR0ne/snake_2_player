const httpServer = require('http').createServer()
const {createGameState, initGame, gameLoop, getUpdatedVelocity} = require('./game')
const {FRAME_RATE} = require('./constants.js')
const state = {}
const clientRooms = {}

const io = require('socket.io')(httpServer, {
    cors: {
        origin: '*',
        methods: ['GET', 'POST'],
        credentials: true,
    },
})

io.on('connection', (client) => {
    client.leave(client.id)
    client.join('snake')

    client.on('keydown', handleKeydown)
    client.on('newGame', handleNewGame)
    client.on('joinGame', handleJoinGame)

    function handleJoinGame(gameCode) {
        let roomName = 'snake'
        state[roomName] = initGame()

        //let allUsers
        //if (room) {
        //console.log('There is a Room!')
        //allUsers = room.sockets
        //}

        //let numClients = 0
        //if (allUsers) {
        //numClients = Object.keys(allUsers).length
        //console.log(`There are ${numClients} number of rooms.`)
        //}

        //if (numClients === 0) {
        //client.emit('unknownGame')
        //return
        //} else if (numClients > 1) {
        //client.emit('tooManyPlayers')
        //return
        //}

        //clientRooms[client.id] = gameCode

        client.join('snake')
        client.number = 2
        client.emit('init', 2)

        startGameInterval(gameCode)
    }

    function handleNewGame() {
        let roomName = 'snake'
        //console.log(roomName)
        client.emit('gameCode', roomName)

        state[roomName] = initGame()

        client.join(roomName)
        client.number = 1
        client.emit('init', 1)
    }

    function handleKeydown(keyCode) {
        const roomName = clientRooms[client.id]

        if (!roomName) {
            return
        }

        try {
            keyCode = parseInt(keyCode)
        } catch (e) {
            console.error(e)
            return
        }

        const vel = getUpdatedVelocity(keyCode)

        if (vel) {
            state[roomName].players[client.number - 1].vel = vel
            //state.player.vel = vel
        }
    }

    //startGameInterval(client, state)
})

function startGameInterval(roomName) {
    const intervalId = setInterval(() => {
        const winner = gameLoop(state[roomName])

        if (!winner) {
            emitGameState(roomName, state[roomName])
        } else {
            emitGameOver(roomName, winner)
            state[roomName] = null
            clearInterval(intervalId)
        }
    }, 1000 / FRAME_RATE)
}

function emitGameState(roomName, state) {
    io.sockets.in(roomName)
        .emit('gameState', JSON.stringify(state))
}

function emitGameOver(roomName, winner) {
    io.sockets.in(roomName)
        .emit('gameOver', JSON.stringify({winner}))
}

io.listen(3000)
